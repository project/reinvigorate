
=========================== Reinvigorate ============================

Reinvigorate is a simple, real-time web analytics + heatmaps solution
that measures your influence on the web. This module provides an easy
to use interface to adding reinvigorate tracking to your Drupal site.

See more on the http://www.reinvigorate.net/

This module is maintained by http://www.devkinetic.com

= Setup =
  1) Upload the module to you site.
  2) Enable the module.
  3) Visit admin/settings/reinvigorate to configure.
   
